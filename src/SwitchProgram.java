
public class SwitchProgram {

	public static void main(String[] args) {
		int a=2;
		
		switch (a) {
		case 1 :
			System.out.println("jan");
			break;
		case 2:
			System.out.println("Feb");
			break;
		case 3 : 
			System.out.println("Mar");
			break;
			
		default :
			System.out.println("Not a valid case");
		
		}

	}

}
