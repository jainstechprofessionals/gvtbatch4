package Day2;

public class LoopsInJava {

	public static void main(String[] args) {

		
		// Entry control (While,For)
		// Exit control (Do while)

//		int i=2;

		/*
		 * while(i<=10) {
		 * 
		 * System.out.println(i); i++; }
		 */

		/*
		 * while(i<=20) { System.out.println(i); i=i+2; }
		 */

		/*
		 * for(int i=2;i<=20;i=i+2) { System.out.println(i); }
		 */
		/*
		 * int i=2;
		 * 
		 * do { System.out.println(i); i=i+2; }while(i<=20);
		 */

		/*
		 * for (int i = 0; i < 5; i++) { for (int j = 0; j < 5; j++) {
		 * System.out.print("*"); } System.out.println(); }
		 */

		for (int i = 0; i < 5; i++) {
			for (int j = i; j >= 0; j--) {
				
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
